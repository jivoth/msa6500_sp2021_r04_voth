from mrjob.job import MRJob
from mrjob.step import MRStep
import re

WORD_RE = re.compile(r"[\w']+")


class MRWordsC(MRJob):

    def steps(self):
        return [
            MRStep(mapper=self.mapper_get_words,
                   combiner=self.combiner_count_words,
                   reducer=self.reducer_count_words)
        ]

    def mapper_get_words(self, _, line):
        # yield each word in the line that starts with "c"
        for word in WORD_RE.findall(line):
            if word.startswith("c"):
                yield (word.lower(), 1)

    def combiner_count_words(self, word, counts):
        # sum the words we've seen so far
        yield (word, sum(counts))

    def reducer_count_words(self, word, counts):
        # yield sums of all the words that start with "c"
        yield (sum(counts), word)


if __name__ == '__main__':
    MRWordsC.run()