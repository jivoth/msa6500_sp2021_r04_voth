# Jonathan Voth

## Master of Science in Analytics

**What commands did you use to run each script?**

The first and second jobs were given to us, so I already had a template of the script. To run the third job, I used the command 'word.startswith("c")' in the Map function to only get words that start with the letter 'c'. Then, from the script template, the key/value pairs were combined, and the Reduce function summed the key/value pairs.

For the fourth job, I used an 'if' command in the Reduce function. So, if the length of the word was greater than or equal to five characters, the key/value pairs were summed. Initially, I put this command in the Map function which yielded the same result, but I read on Canvas to change the Reduce function. I guess either way works. This is another example of how one job can be executed multiple ways.

**What technical errors did you experience?**

The first technical error I ran into was updating my repository to reflect the changes that were made. For some reason, I couldn't get my computer to merge the projects.

The technical errors I experienced with the actual reflection dealt with proper syntax. I knew the goal of the assignment, but I had a hard time actually writing the code to execute it properly. I experienced object errors, objects not being defined, syntax errors like missing commas or parantheses, invalid arguments, etc. Sometimes I had to look up the error to see what was actually happening and how to fix it. Being new to python, for the most part I was guessing and checking throughout the assignment.

**What conceptual difficulties did you experience?**

I don't feel I had any major conceptual difficulties this week. As I went through the videos, I was following everything perfectly and able to execute the jobs on my computer as well. The one thing I struggled with was figuring out where to alter the commands to run the variations of the script. I knew there would be multiple ways to do this, but I just needed to figure out one. I had to read on the mrjobs website about the contruction of each command and what it was actually doing to make the job run. Once I was able to decipher the code, I could change the appropriate parts to achieve the goal of the assignment.

**How much time did you spend on each part of the assignment?**

Overall, I spent around 5-6 hours on the assignment this week. Each week takes less and less time as I get more comfortable operating in this environment. Getting Docker running takes very little time, almost negligible. For this particular assignment, I spent the most time learning about the contruction of the mrjob scripts, specifically, what each step was doing. I needed to understand what was happening in order to alter the code to change the job. This took me a couple hours, but I figured it out. It was a great moment when my code executed the job properly. I was excited to see it go through.

Some other areas that took up some time were merging the two projects to reflect the updated documents. I had to work around some errors there. Also, watching and learning from the videos took time too. Sometimes, I run into an error that was covered in a previous module, so I have to go back to that video to find the fix. I think if I run into the same error enough times, I'll remember how to work through it.

**What was the hardest part of this assignment?**

The hardest part of the assignment for me was deciphering the python code to execute the new jobs. I spent a decent amount of time reading through the mrjob website to learn and doing some research of my own. This translated into many trial and error runs of my code to see of the job went through. I would fix one part, run it, if a new error came up, then I would go back and fix that, and repeat until it went through. I did not have any trouble at all with goal of the assignment. It was more writing the proper code to execute the job.

**What was the easiest part of this assignment?**

One of the easiest parts of the assignment was running the first two jobs. The script was already provided for us through mrjobs. I also had an easy time following this assignment from a concept perspective. From the start, I knew what I needed to do. The video tutorials did a very nice job explaining the objective of the assignment and a few of the things that can be done with MapReduce. I haven't always been able to understand the objective from the start of the week. This week, however, there was a slight sense of relief as I began the assignment. I enjoyed learning about MapReduce as I know it plays a big role in the business world.

**What did you actually learn from doing this assignment?**

Simply put, I learned how to run a MapReduce job in HDFS using python commands. The biggest takeaway from this week for me was increasing my usage and knowledge with python. I actually had to understand the script and change it to assess my needs. You can't do that unless you understand the language. I had to research more about the structure, defining functions and objects, proper arguments, etc. It was all a learning experience. This week I was able to build on what I learned last week and will continue to do so in the future.

**Why does what I learned matter both academically and practically?**

The process we learned this week is very practical. The input file we used was just randomly generated text, so the output wasn't meaningful. However, imagine if that file were customer reviews of a product or service and you want to find out how many times a certain word appears in the text, then the job has meaning. And this week, the job we performed was simple. I can only assume jobs get more complicated. We have been building up to this week in previous assignments and will build upon this in future assignments. I also increased my usage of python which, as mentioned previously, has positive implications in an academic and practical sense. Map and Reduce are two functions widely used in the professional world. It is vital that we get experience doing simple jobs to get some exposure as we get ready to enter the workforce.
